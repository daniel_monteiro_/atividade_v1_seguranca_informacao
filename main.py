# coding=utf-8
"""

FBuni
Disciplina de Segurança da Informação
Prof Helano
Aluno: Daniel Monteiro

"""

from substituicao import cifra_substituicao, decifra_substituicao
from transposicao import cifra_transposicao, decifra_transposicao
from uso_unico import cifra_uso_unico, decifra_uso_unico

# #  prova/teste substituicao:

# k_sub = int(input("Chave de Deslocamento Para Cifra de SUBSTIUIÇÃO(número inteiro):"))
# palavra_sub = str(input("Palavra para decifrar:"))
# cifra_sub = cifra_substituicao(palavra_sub, k_sub)
# print(cifra_sub)
# print(decifra_substituicao(cifra_sub, k_sub*-1))

# # fim prova/teste substituicao


# prova/teste trsnposição:

# k_transpos = str(input("Chave de Deslocamento Para a Cifra de TRANSPOSIÇÃO(em texto):"))
# palavra_transpos = str(input("Palavra ou frase TRANSPOSIÇÃO:"))
# cifra_transp = cifra_transposicao(palavra_transpos, k_transpos)
# print(cifra_transp)
# print(decifra_transposicao(cifra_transp, k_transpos))

# print(cifra_transposicao("VAMOS EMBORA, FOMOS DESCOBERTOS", ks)) #  ks = ZEBRAS
# cifra3 = cifra_transposicao("please transfer one million dollars to my swiss bank account six two two four", ks) # ks = MEGABUCK
# print(cifra3)  # ks = MEGABUCK

# FIM prova/teste trsnposição

# prova/teste uso único:
# texto_cifrado_binario, chave, texto_cifrado = cifra_uso_unico()

# texto_decifrado = decifra_uso_unico(texto_cifrado_binario, chave)

# print("Texto Cifrado: " + str(texto_cifrado))
# print("Texto decifrado: " + str(texto_decifrado))
# FIM prova/teste uso único


# CABECALHO:
print("FBuni\nDisciplina de Segurança da Informação\nProfessor Helano\nAluno: Daniel Monteiro\n\n")

# Execução dos três métodos:

# SUBSTIUIÇÃO:
try:
    k_sub = int(input("Chave de Deslocamento Para Cifra de SUBSTIUIÇÃO(número inteiro):"))
    palavra_sub = str(input("Palavra para decifrar:"))
    cifra_sub = cifra_substituicao(palavra_sub, k_sub)
    print(cifra_sub)
    print(decifra_substituicao(cifra_sub, k_sub*-1))
    print()
except Exception as e:
    print("Uma pequena exceção rara na cifra de SUBSTIUIÇÃO. Pode tentar de novo com outros textos e chaves?")


# TRANSPOSIÇÃO:
try:
    k_transpos = str(input("Chave de Deslocamento Para a Cifra de TRANSPOSIÇÃO(em texto):"))
    palavra_transpos = str(input("Palavra ou frase TRANSPOSIÇÃO:"))
    cifra_transp = cifra_transposicao(palavra_transpos, k_transpos)
    print(cifra_transp)
    print(decifra_transposicao(cifra_transp, k_transpos))
    print()
except Exception as e:
    print("Uma pequena exceção rara na cifra de TRANSPOSIÇÃO. Pode tentar de novo com outros textos e chaves?")


# USO ÚNICO:
try:
    texto_cifrado_binario, chave, texto_cifrado = cifra_uso_unico()

    texto_decifrado = decifra_uso_unico(texto_cifrado_binario, chave)

    print("Texto Cifrado: " + str(texto_cifrado))
    print("Texto decifrado: " + str(texto_decifrado))
except Exception as e:
    print("Uma pequena exceção rara na cifra de USO ÚNICO. Pode tentar de novo com outros textos e chaves?")


print()
print("Execute novamente para novos testes.")
