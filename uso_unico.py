# coding=utf-8

# print("Operação de binários com Python: https://wiki.python.org.br/BitwiseOperators")


def string_to_binary(s):
    res = ''.join(format(ord(i), 'b') for i in s)
    return res


def binario_para_decimal(binary):
    string = int(binary, 2)
    return string


def binary_to_string(s):
    str_data = ' '

    # slicing the input and converting it
    # in decimal and then converting it in string
    for i in range(0, len(s), 7):
        temp_data = s[i:i + 7]

        decimal_data = binario_para_decimal(temp_data)

        str_data = str_data + chr(decimal_data)

    return str_data.replace("kkaulall", " ")


def cifra_uso_unico():
    print("---------- CIFRA DE USO ÚNICO ----------")
    texto_cifrar = str(input("Digite o texto que você quer cifrar:"))
    chave_cifrar = str(input("Digite uma palavra para ser a chave: "))

    texto_cifrar = texto_cifrar.replace(" ", "kkaulall")  # porque espaço atrapalha na conversão de binários. Quando decifrar mudar para espaço vazio novamente.
    chave_cifrar = chave_cifrar.replace(" ", "kkaulall")  # porque espaço atrapalha na conversão de binários. Quando decifrar mudar para espaço vazio novamente.

    binario_texto = string_to_binary(texto_cifrar)
    binario_chave = string_to_binary(chave_cifrar)

    if len(binario_texto) > len(binario_chave):
        sobra = len(binario_texto) - len(binario_chave)

        str_zeros = ""
        for i in range(int(sobra)):
            str_zeros += "1"  # eu preferi colocar 1 para completar porque cifra melhor. Parece que 0 é meio que nulo e com textos grandes é mesmo que nada

        binario_chave = str_zeros + binario_chave

    elif len(binario_texto) < len(binario_chave):
        sobra = len(binario_chave) - len(binario_texto)

        str_zeros = ""
        for i in range(int(sobra)):
            str_zeros += "1"  # eu preferi colocar 1 para completar porque cifra melhor. Parece que 0 é meio que nulo e com textos grandes é mesmo que nada

        binario_texto = str_zeros + binario_texto

    # Fazer o XOR entre os binários:
    str_xor_saida = ""  # palavra decifrada
    for bc, bt in zip(binario_chave, binario_texto):
        if (bc == "1" and bt == "1") or (bc == "0" and bt == "0"):  # de acrodo com a tabela XOR, a saída é zero
            str_xor_saida += "0"
        else:  # 0 e 1 ou 1 e 0 a saída é 1
            str_xor_saida += "1"

    return str_xor_saida, binario_chave, binary_to_string(str_xor_saida)


def decifra_uso_unico(binario_texto_cifrado, chave_binario):
    print("---------- DECIFRAR CIFRA DE USO ÚNICO ----------")

    str_xor_saida = ""
    for t, c in zip(binario_texto_cifrado, chave_binario):
        if (t == "1" and c == "1") or (t == "0" and c == "0"):  # de acrodo com a tabela XOR, a saída é zero
            str_xor_saida += "0"
        else:  # 0 e 1 ou 1 e 0 a saída é 1
            str_xor_saida += "1"

    texto_decifrado = binary_to_string(str_xor_saida)

    return texto_decifrado
