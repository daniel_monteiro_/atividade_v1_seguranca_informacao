# coding=utf-8
alfabeto_e_numeros = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9']


def get_index_letter(c):
    index = 0
    for a in alfabeto_e_numeros:
        if a == c:
            break
        index += 1

    return index


def cifra_substituicao(texto, k):
    print("---------- CIFRA DE SUBSTITUIÇÃO ----------")
    texto = texto.lower()

    texto_criptografado = ""
    index = k

    for c in texto:
        index_letter = get_index_letter(c)
        while index > len(alfabeto_e_numeros) - 1:
            index = k - (len(alfabeto_e_numeros))

        index = abs(index)

        index_get = index_letter + index

        while index_get > len(alfabeto_e_numeros) - 1:
            index_get = index_get - (len(alfabeto_e_numeros))

        index_get = abs(index_get)

        texto_criptografado += alfabeto_e_numeros[index_get]

    return texto_criptografado


def decifra_substituicao(texto, k):
    print("---------- DECIFRAR CIFRA DE SUBSTITUIÇÃO ----------")
    texto_descriptografado = ""
    index = k
    for t in texto:
        index_letter = get_index_letter(t)

        while index > len(alfabeto_e_numeros):
            index = k - len(alfabeto_e_numeros)

        index = abs(index)
        texto_descriptografado += alfabeto_e_numeros[index_letter - index]

    return texto_descriptografado
