# coding=utf-8
alfabeto_e_numeros = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9']


class LetterOrder:
    position = -1
    letter = ''
    position_on_list = -1


# metodo de pegar as palavras em ordem alfabetica
def get_list_ordem_palavras(chave):
    list_index_order = []
    i = 0
    for c in chave:
        position = 0
        for l in alfabeto_e_numeros:
            if c == l.upper():
                letter_order = LetterOrder()
                letter_order.position = position
                letter_order.letter = c
                letter_order.position_on_list = i

                list_index_order.append(letter_order)

                position = 0
                i += 1
            position += 1

    for j in range(len(list_index_order)-1, 0, -1):
        for k in range(j):
            if list_index_order[k].position > list_index_order[k+1].position:
                temp = list_index_order[k]
                list_index_order[k] = list_index_order[k+1]
                list_index_order[k+1] = temp

    return list_index_order


get_list_ordem_palavras("ZEBRAS")


