# coding=utf-8
alfabeto_e_numeros = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '/']

import random
import math


# classe para ser usada na criptografia por transposiçao no momento que vai organizar as colunas por ordem alfabetica
# da chave
class LetterOrder:
    position = -1
    letter = ''
    position_on_list = -1


class ColumnOrderLetter:
    letter = ''
    list_column = []


def get_list_ordem_palavras(chave):
    list_index_not_order = []
    list_index_order = []
    i = 0
    for c in chave:
        list_index_not_order.append(c)
        position = 0
        for l in alfabeto_e_numeros:
            if c.lower() == l.lower():
                letter_order = LetterOrder()
                letter_order.position = position
                letter_order.letter = c
                letter_order.position_on_list = i

                list_index_order.append(letter_order)

                position = 0
                i += 1
            position += 1

    for j in range(len(list_index_order) - 1, 0, -1):
        for k in range(j):
            if list_index_order[k].position > list_index_order[k + 1].position:
                temp = list_index_order[k]
                list_index_order[k] = list_index_order[k + 1]
                list_index_order[k + 1] = temp

    return list_index_order, list_index_not_order


def there_is_letter(c, list_):
    if c in list_:
        return True
    return False


def get_letters_resting(qtd):
    letters = ''

    for i in range(qtd):
        r = random.randint(0, 25)
        letters += alfabeto_e_numeros[r].lower()

    return letters


def cifra_transposicao(texto, chave):  # 2
    print("---------- CIFRA DE TRANSPOSIÇÃO ----------")
    texto = texto.replace(" ", "").replace(",", "").replace(".", "").lower()
    chave = chave.lower()

    qtd_colunas = len(chave)
    qtd_colunas = int(qtd_colunas)

    # formando as colunas
    lista_palavras = []
    i = 1
    lista_palavra = []
    it = 0
    for t in texto:
        lista_palavra.append(t)

        if i == qtd_colunas:
            lista_palavras.append(lista_palavra)
            lista_palavra = []
            i = 0

        if it == (len(texto) - 1) and qtd_colunas > i > 0:
            qtd_resto = abs(qtd_colunas - i)
            letters = get_letters_resting(qtd_resto)
            for l in letters:
                lista_palavra.append(l)
            lista_palavras.append(lista_palavra)
            lista_palavra = []
            i = 0

        i += 1
        it += 1
    # FIM formando as colunas

    qtd_linhas = math.floor(len(texto)/len(chave))

    soma_correta = 0
    if qtd_linhas == qtd_colunas:
        soma_correta -= 1
    elif qtd_linhas < qtd_colunas:
        soma_correta = abs(qtd_colunas - qtd_linhas)
        soma_correta -= 1

    # formando linhas pelas colunas
    lista_transposicao = []
    # lp = lista_palavras[ilp]
    lista_palavra_coluna = []
    if qtd_linhas == qtd_colunas or qtd_linhas < qtd_colunas:
        i = 0
        while i <= (qtd_linhas+soma_correta):  # Tratar no caso de linha maior que coluna
            for p in lista_palavras:
                lista_palavra_coluna.append(p[i])
            lista_transposicao.append(lista_palavra_coluna)
            lista_palavra_coluna = []

            i += 1
    else:  # linhas maiores que as colunas
        i = 0
        while i < qtd_linhas:
            j = 0

            while j < qtd_colunas:
                for p in lista_palavras:
                    lista_palavra_coluna.append(p[j])
                lista_transposicao.append(lista_palavra_coluna)
                lista_palavra_coluna = []

                j += 1
            i += 1

        lista_transposicao = lista_transposicao[0:qtd_colunas]

    # FIM formando as linhas pelas colunas

    # reordenar conforme a chave em ordem alfabética
    list_palavra_em_ordem, list_palavra_nao_ordem = get_list_ordem_palavras(chave)
    list_cript = []
    if qtd_linhas <= qtd_colunas:
        i = 0
        while i < len(chave):
            for po in list_palavra_em_ordem:
                ipno = 0
                for pno in list_palavra_nao_ordem:
                    if po.letter == pno:
                        letterColumn = ColumnOrderLetter()
                        letterColumn.letter = po.letter
                        letterColumn.list_column = lista_transposicao[ipno]  # da erro com index 5

                        list_cript.append(letterColumn)
                        i += 1
                        break

                    ipno += 1
    else:  # qtd linhas maior que colunas
        for po in list_palavra_em_ordem:
            ipno = 0
            for pno in list_palavra_nao_ordem:
                if po.letter == pno:
                    letterColumn = ColumnOrderLetter()
                    letterColumn.letter = po.letter
                    letterColumn.list_column = lista_transposicao[ipno]  # da erro com index 5

                    list_cript.append(letterColumn)
                    break

                ipno += 1

    # FIM reordenar conforme a chave em ordem alfabética

    # criar variável que será retornada com a cripotografia
    str_return = ""
    for cript in list_cript:
        str_return += str(cript.list_column).replace("[", "").replace("]", "").replace(",", "")\
                          .replace("\'", "").replace(" ", "") + " "
    # FIM criar variável que será retornada com a cripotografia

    return str_return


# Para a decifrar, o destinatário tem apenas que dividir o comprimento da mensagem (30) pelo da chave (6), e ler as
# colunas pela ordem das letras da chave
def decifra_transposicao(cifra, chave):
    print("---------- DECIFRAR CIFRA DE TRANSPOSIÇÃO ----------")

    cifra = str(cifra).replace(" ", "").lower()
    chave = chave.lower()

    qtd_colunas = len(chave)

    qtd_linhas = math.floor(len(cifra)/qtd_colunas)

    lista_cifra_divida = []

    soma_correta = 0
    if qtd_linhas < qtd_colunas:
        soma_correta = abs(qtd_colunas - qtd_linhas)
    elif qtd_linhas >= qtd_colunas:
        soma_correta += 1

    count = 0
    lista_coluna_cifra = []
    count_t = 0
    for c in cifra:  # encontrar termo definitivo para dividir as palavras
        lista_coluna_cifra.append(c)

        if count == qtd_linhas-soma_correta:
            lista_cifra_divida.append(lista_coluna_cifra)
            lista_coluna_cifra = []
            count = 0
            continue

        count += 1
        count_t += 1

    if [] in lista_cifra_divida:
        lista_cifra_divida.remove([])

    # if len(lista_cifra_divida) > len(chave):
    #     last_item = lista_cifra_divida.__getitem__(len(lista_cifra_divida)-1)
    lista_palavra_em_ordem, lista_palavra_nao_ordem = get_list_ordem_palavras(chave)

    list_coumn_and_letters = []
    for lcc, po in zip(lista_cifra_divida, lista_palavra_em_ordem):
        columnLetter = ColumnOrderLetter()
        columnLetter.letter = po.letter
        columnLetter.list_column = lcc

        list_coumn_and_letters.append(columnLetter)

    # desordenar da ordem alfabetica (depois, formar colunas pelas linhas). Remontar a lista com a palavra.
    list_desordenada = []  # nome normal da chave
    i = 0
    while i < len(chave):
        for c in chave:
            for column in list_coumn_and_letters:
                if c == column.letter and not there_is_letter(column.letter, list_desordenada):
                    list_desordenada.append(column)
                    i += 1

    # FIM desordenar do ordem alfabetica (depois, formar colunas pelas linhas)

    # formar as colunas pelas linhas / obter a string decifrada
    diferenca = 0
    range_for = 0
    if qtd_linhas <= qtd_colunas:
        diferenca = abs(qtd_colunas - qtd_linhas)
        range_for = len(list_desordenada)-diferenca
    else:
        range_for = qtd_linhas
    i = 0
    strReturn = ""
    for c in range(range_for): # aqui tem um erro. Na verdade tem que dimunuir pela diferenca do tamanho da coluna pelas linhas e nao somente por 1
        for column in list_desordenada:
            strReturn += column.list_column[i]
        i += 1
    # FIM formar as colunas pelas linhas / obter a string decifrada

    return strReturn
